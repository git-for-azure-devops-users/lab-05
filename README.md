# Git for TFS users
Lab 05: Working with multiple features (simultaneously)

---

# Tasks

 - Create a new local repository from visual studio
 
 - Create a new remote repository to push your changes
 
 - Create and work in a feature branch
 
 - Work with multiple feature branches

---

## Create a new repository from visual studio

 - Create a new local repository from visual studio:

&nbsp;
<img alt="Image 1.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/createnewrepovs.png?view=azure-devops" border="1">
&nbsp;

- Click the repository, add a new solution and commit the changes:

&nbsp;
<img alt="Image 1.2" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/share-your-code-in-git-vs-2017/vs-commit-changes.png?view=azure-devops" border="1">
&nbsp;

---

## Create a new remote repository to push your changes

 - Create a new repository calle "lab-05-<your-user>" in the TFS web portal:

&nbsp;
<img alt="Image 2.1" src="https://beeming.net/image/1000/0/07BF3FC49275C8AE0D09A1C473C1CCD239CB9102/07BF3FC49275C8AE0D09A1C473C1CCD239CB9102.png" border="1">
&nbsp;

- To configure your local repo to push to your remote repo, go the Settings page in Team Explorer, select Repository Settings, and under Remotes, select Add.

&nbsp;
<img alt="Image 2.2" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/add_remote_vs.png?view=azure-devops" border="1">
&nbsp;

- Push your local changes to your remote repository:

&nbsp;
<img alt="Image 2.3" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/push-to-origin.png?view=azure-devops" border="1">
&nbsp;

---

## Create and work in a feature branch

 - Create a new feature branch called "feature/first":

&nbsp;
<img alt="Image 3.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/new-local-branch-menu.png?view=azure-devops" border="1">
&nbsp;

 - Perform some changes WITHOUT commit your changes

 - Try to checkout the master branch, you will receive the error "cannot switch to master"

&nbsp;
<img alt="Image 3.2" src="https://2.bp.blogspot.com/-_RUvcTxNu7E/V5ti5Xb5n8I/AAAAAAAAASY/UTBE01XrG4IJagUoqf3nSf3IpU1OudocACLcB/w1200-h630-p-k-no-nu/switch%2Bbranch%2Berror.PNG" border="1">
&nbsp;

- Commit your changes and checkout the master branch

---

## Work with multiple feature branches

 - Create a new feature branch called "feature/second" based on "master":

&nbsp;
<img alt="Image 4.1" src="https://docs.microsoft.com/en-us/azure/devops/repos/git/_img/gitquickstart-vs2017/new-local-branch-menu.png?view=azure-devops" border="1">
&nbsp;

 - Perform some changes WITHOUT commit your changes
 
 - Try to checkout the "feature/first" branch, you will receive the error "cannot switch to feature/first"

&nbsp;
<img alt="Image 4.2" src="https://2.bp.blogspot.com/-_RUvcTxNu7E/V5ti5Xb5n8I/AAAAAAAAASY/UTBE01XrG4IJagUoqf3nSf3IpU1OudocACLcB/w1200-h630-p-k-no-nu/switch%2Bbranch%2Berror.PNG" border="1">
&nbsp;

- This time we will stash our changes, open the "Command Prompt" from the "Action" button and run:
```
$ git stash
```

 - Then, checkout the "feature/first" branch, perform some changes and commit them

 - Finally checkout to the "feature/second" and unstash your changes from the command prompt
```
$ git stash apply
```
